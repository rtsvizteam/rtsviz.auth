rtsviz.auth
====


Authorization Management For 'rtsviz' Package.
===


A tool to create and manage users and credentials for the rtsviz package.


Installation:
===

To install the development version run following code:

```R
remotes::install_bitbucket("rtsvizteam/rtsviz.auth")
```

The [CRAN](https://cran.r-project.org) version coming soon.


Example
===

```R

	library(rtsviz.auth)

	# create sample authorization list
	storage = create.db.auth()
	auth = create.user('demo','demo.password')
	auth = c(auth, create.user('test','test.password'))
	auth = c(auth, create.user('sample','sample.password'))
	print(auth)
	
	storage$exists('rtsvis.demo.app')
	
	# save
	storage$save(auth, 'rtsvis.demo.app')
	
	# load
	auth = storage$load('rtsvis.demo.app')
	print(auth)
```
