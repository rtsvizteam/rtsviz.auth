###############################################################################
# Common functions used internally
###############################################################################
len = function(x) length(x)

mlast = function(m, n=1) if( is.matrix(m) ) m[(nrow(m)-n+1):nrow(m), ,drop=FALSE] else m[(len(m)-n+1):len(m)]

mlag = function(m, nlag=1) 
  if( is.matrix(m) ) {
    n = nrow(m)
    if(nlag > 0) {
      m[(nlag+1):n,] = m[1:(n-nlag),]
      m[1:nlag,] = NA
    } else if(nlag < 0) {
      m[1:(n+nlag),] = m[(1-nlag):n,]
      m[(n+nlag+1):n,] = NA
    } 
	m
  } else { # vector
    n = len(m)
    if(nlag > 0) {
      m[(nlag+1):n] = m[1:(n-nlag)]
      m[1:nlag] = NA
    } else if(nlag < 0) {
      m[1:(n+nlag)] = m[(1-nlag):n]
      m[(n+nlag+1):n] = NA
    }
	m	
  }
  
spl = function(s, delim = ',') unlist(strsplit(s,delim))

rep.row = function(m, nr) matrix(m, nrow=nr, ncol=len(m), byrow=TRUE)

trim = function(s) sub('\\s+$', '', sub('^\\s+', '', s))

make.copy = function(x, default) { out = x; out[] = default; out }

iif = function(cond, truepart, falsepart) 
	if(cond) truepart else falsepart 

ifnull = function(x, y) iif(is.null(x), y, x)

ifna = function(x, y) iif(is.na(x) | is.nan(x) | is.infinite(x), y, x)

to.date = function(x) if(class(x)[1] != 'Date') as.Date(x, format='%Y-%m-%d') else x


# capture cat function output to string
cat2str = function(..., sep=',') {
	file = rawConnection(raw(0L), open='w')

	cat(..., file = file, sep = sep)	
	out = rawToChar(rawConnectionValue(file))
	
	close(file)
	file = NULL
	
	out
}


# set global options
set.options = function(key, ..., overwrite=TRUE) {
	values = list(...)
	if( len(values) == 1 && is.null(names(values))) values = values[[1]]
	temp = ifnull(options()[[key]], list())
	
	for(i in names(values))
		if(overwrite)
			temp[[i]] = values[[i]]
		else {
			if( is.null(temp[[i]]) )
				temp[[i]] = values[[i]]
		}
	
	options(make.list(key, temp))
}

# make list
make.list = function(key, value) {
	out = list()
	out[[key]] = value
	out
}


# --------------------------------
# load default option
# 
# use following waterfall
# 1. options
# 2. environment
# 3. default option
# 
# more details at
# http://blog.revolutionanalytics.com/2015/11/how-to-store-and-use-authentication-details-with-r.html
# --------------------------------
get.default.option = function(name, default) {
	# try options first
	if (is.null(getOption(name))) { 
	
		# try environment variables next
		value = Sys.getenv(name, unset=NA)
		
		# use default is missing
		if (is.na(value)) 
			options(make.list(name, default))
		else
			options(make.list(name, value))
	}
	getOption(name)
}	

