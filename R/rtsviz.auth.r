#' @title `rtsviz.auth` - Authorization Management For 'rtsviz' Package.
#' 
#' @description A tool to create and manage users and credentials for the rtsviz package.
#' 
#' 
#' @examples
#'  # small toy example
#'
#'  # create sample authorization list
#'  storage = create.csv.auth()
#'  auth = create.user('demo','demo.password')
#'  auth = c(auth, create.user('test','test.password'))
#'  auth = c(auth, create.user('sample','sample.password'))
#'  print(auth)
#'
#'  # save
#'  storage$save(auth, 'rtsvis.demo.app')
#'  
#'  # load
#'  auth = storage$load('rtsvis.demo.app')
#'  print(auth)
#' 
#' 
#' @name rtsviz.auth
#' @docType package
#' 
NULL




###############################################################################
#' Default location to save data
#'
#' The following waterfall is used to lookup the default location: 
#' 1. options
#' 2. environment
#' 3. default option
#' 
#' Good practice is not to store this setting inside the script files. 
#' Add options(RTSVIZ_AUTH_DATA_FOLDER='C:/Auth') line to the .Rprofile to use 'C:/Auth' folder.
#'
#' @return default location to save data
#'
#' @examples
#' 	# Default location to save data
#' 	ra.default.location()
#'
#' @export
#' @rdname DefaultStorageOptions
###############################################################################
ra.default.location = function() get.default.option( 'RTSVIZ_AUTH_DATA_FOLDER', tempdir() )


###############################################################################
#' Default database to save data
#'
#' Good practice is not to store this setting inside the script files. 
#' Add options(RTSVIZ_AUTH_DB='mongodb://localhost') line to the .Rprofile to use 'mongodb://localhost' database.
#'
#' @return default database to save data
#'
#' @examples
#' 	# Default location to save data
#' 	ra.default.database()
#'
#' @export
#' @rdname DefaultStorageOptions
###############################################################################
ra.default.database = function() get.default.option('RTSVIZ_AUTH_DB', 'mongodb://localhost')


###############################################################################
#' MongoDB GridFS Storage model
#'
#' @param url address of the mongodb server in mongo connection string URI format, 
#' \strong{defaults to ra.default.database database}. 
#' 
#' For local mongodb server, use 'mongodb://localhost' URI.
#' For local authenticated mongodb server, use 'mongodb://user:password@localhost' URI.
#' @param db name of database, \strong{defaults to 'data_storage'}
#'
#' @return list with storage options
#'
#' @examples
#'  \donttest{ 
#'   create.db.auth()
#'  }
#'
#' @export
###############################################################################
create.db.auth = function
(
	url = ra.default.database(),
	db = 'auth_data_storage'	
) {
	# gridfs functionality is available since mongolite 1.5.9 version
	if (utils::packageVersion('mongolite') < '1.5.9')
		stop('mongolite >= 1.5.9 needed for this function.', call. = FALSE)

	# connect to db; if db is not setup, mongo will create empty one
	fs = mongolite::gridfs(db = db, url = url)
	
	# check saved files and remove all duplicates
	index = fs$find()$name
	index = index[duplicated(index)]
	
	# remove all duplicates
	if( len(index) > 0 )
		for(i in index)
			fs$remove(i)
	
	# MongoDB storage definition
	list(
		fs = fs,
		load = function(id) {
			temp = fs$read(id)$data
			tryCatch(unserialize(brotli::brotli_decompress(temp)), error=function(e) unserialize(temp))
		},
		save = function(auth.data, id) {
			# the only way to update data is to remove the old file
			try(fs$remove(id), silent = TRUE)
			fs$write(brotli::brotli_compress(serialize(auth.data, NULL), 2), id)
		},
		exists = function(id) sum(fs$find()$name == id) > 0
	)
}    



###############################################################################
#' Rdata file Storage model
#'
#' @param location storage location, \strong{defaults to ra.default.location folder}
#' @param extension file extension, \strong{defaults to 'Rdata'}
#'
#' @return list with storage options
#'
#' @examples
#'  create.rdata.auth()
#'
#' @export
###############################################################################
create.rdata.auth = function
(
	location = ra.default.location(),
	extension = 'Rdata'
)
	list(
		load = function(id) { 
			auth.data = NULL; 
			filename = file.path(location, paste0(id, '.', extension))
			tryCatch({
				temp = readBin(filename, raw(), file.info(filename)$size)
				unserialize(brotli::brotli_decompress( temp ))
			}, error=function(e) {load(filename); auth.data})
		},
		save = function(auth.data, id) {
			check.create.folder(location) 
			filename = file.path(location, paste0(id, '.', extension))
			writeBin(brotli::brotli_compress(serialize(auth.data, NULL), 2), filename);
		},
		exists = function(id) {
			filename = file.path(location, paste0(id, '.', extension))
			tryCatch(file.exists(filename), error=function(e) FALSE)
		},		
		location = location,
		extension = extension
	)

	
###############################################################################
#' CSV file Storage model
#'
#' @param location storage location, \strong{defaults to ra.default.location folder}
#' @param extension file extension, \strong{defaults to 'csv'}
#'
#' @return list with storage options
#'
#' @examples
#'  create.csv.auth()
#'
#' @export
###############################################################################
create.csv.auth = function
(
	location = ra.default.location(),
	extension = 'csv'
)
	list(
		load = function(id) { 
			filename = file.path(location, paste0(id, '.', extension))
			data = suppressWarnings(utils::read.table(filename, header=FALSE,check.names=FALSE, stringsAsFactors=FALSE, sep=','))
			stats::setNames(as.list(data[,2]), data[,1])
		},
		save = function(auth.data, id) {
			check.create.folder(location) 
			filename = file.path(location, paste0(id, '.', extension))
			suppressWarnings(utils::write.table(as.matrix(auth.data), sep=',',  
			row.names = TRUE, col.names = FALSE, file = filename, append = FALSE, quote = FALSE))
		},		
		exists = function(id) {
			filename = file.path(location, paste0(id, '.', extension))
			tryCatch(file.exists(filename), error=function(e) FALSE)
		},		
		location = location,
		extension = extension
	)
	

# helper function to create folder if it does not exists
check.create.folder = function(location) if(!dir.exists(location)) dir.create(location, TRUE, TRUE) 
	

###############################################################################
#' Create user/password structure
#'
#' @param username user name
#' @param password password
#'
#' @return list with user and password
#'
#' @examples
#'  auth = create.user('demo','demo.password')
#'  auth = c(auth, create.user('test','test.password'))
#'  auth = c(auth, create.user('sample','sample.password'))
#'  print(auth)
#'
#' @export
###############################################################################	
create.user = function(username, password) {
	password = digest::digest(paste0(username, password), algo='sha512',serialize=F)
	stats::setNames(as.list(password), username)
}	

